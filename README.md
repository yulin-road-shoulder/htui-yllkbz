



---

# 一、HtUI是什么

HtUI是基于ElementUI@2.14进行二次开发的一套UI库 并且该文档中的HtUI版本是大于等于@1.3.58。
HtUI源码库请移步[这里](https://gitee.com/yulin-road-shoulder/htui-yllkbz)

---

# 二、使用步骤
## 1.引入库
>代码如下：

```c
yarn add htui-yllkbz
import Htui from "htui-yllkbz"
import "htui-yllkbz/lib/htui.css";
Vue.use(Htui);

```

## 2.组件库
### 1.HtExport--导出文件
>组件使用说明:主要用于附件的导出 将主要的导出方法进行了统一封装处理
```c
 <ht-export method="method" :url="url" :exportBefore="exportBeforeFunc" :fileName="fileName"></ht-export>
```
**Attributes/Events:**
|参数名|说明 | 类型 |可选值|默认值|
|--|--|--|--|--|
|method  | 请求方式(必填) |string | get,post|-|
|url| 请求地址(必填)|string | -|-|
|file-name| 导出文件名,如果不传则取后端传过来的文件名|string | -|-|
|export-before| 是否允许导,默认允许导出|boolean | -|true|

**Slot:**
|名称|说明 | 
|--|--|
|-|自定义导出按钮| 
---
### 2.HtDrawer--抽屉
>组件使用说明:对el-drawer进行了优化和封装 ,默认添加了foot以及对body内容进行滚动条处理
```c
 
    <ht-drawer
      v-model="state.visible"
      :title="title"
      @onOk="onOk"
      @onCancel="onCancel"
    >
     ...
     body
     ...
    </ht-drawer>
```
**Attributes/Events:**
|参数名|说明 | 类型 |可选值|默认值|
|--|--|--|--|--|
|value   | 是否打开弹窗 |boolean| true|false|false|
|title|抽屉名称|string | -|-|
|size|尺寸|string | -|50%|
|direction|弹出方向|string | -|参考element-drawer|
|append-to-body|Drawer 自身是否插入至 body 元素上。嵌套的 Drawer 必须指定该属性并赋值为 true | boolean|true,false|true|
|wrapper-closable| 是否可以通过按下 ESC 关闭 Drawer|boolean | -|false|
|close-on-press-escape| 是否可以通过按下 ESC 关闭 Drawerr|boolean | -|false|
|destroy--on-close| 控制是否在关闭 Drawer 之后将子元素全部销毁|boolean | -|false|
|modal| 是否需要遮罩层|boolean | -|true|
|custom-class| Drawer 的自定义类名|string| -|-|
|modal-append-to-body| 遮罩层是否插入至 body 元素上，若为 false，则遮罩层会插入至 Drawer 的父元素上|boolean | -|true|
|show-close| 是否展示右上角的关闭图标|boolean | -|true|
|with-header| 控制是否显示 header 栏, 默认为 true, 当此项为 false 时, title attribute 和 title slot 均不生效|boolean | -|true|
|with-footer| 控制是否显示 foot 栏, 默认为 true, 当此项为 false 时, foot attribute 和 foot slot 均不生效|boolean | -|true|
|@onOk| 用户点击foot里面的确定按钮时候调用该方法 | Function|-|-|
|@onCancel| 用户点击foot里面的确定按钮时候调用该方法 | Function|-|-|

**Slot:**
|名称|说明 | 
|--|--|
|-|Drawer中body的内容| 
|title|Drawer 标题区的内容| 
|foot|Drawer中foo的内容| 
---
### 3.HtSelectUser--选择用户
>组件使用说明:主要用于在系统中需要选择部门下用户时候或者查看用户时候使用
```c
 <ht-select-user v-model="value"></ht-select-user>
```
**Attributes/Events:**
|参数名|说明 | 类型 |可选值|默认值|
|--|--|--|--|--|
|value/v-model| 人员ID |string | -|-|
|org| 部门id,传入值之后就会根据该值自动过滤成该部门下的用户|string | -|-|
|size| 尺寸|string |medium / small / mini-|-|
|placeholder| 输入框占位文本|string| -|-|
|disabled| 是否禁用|boolean| -|false|
|clearable| 是否可以清除|boolean| -|false|
|multiple| 是否可以多选|boolean| -|false|
|check-strictly| 是否严格的遵守父子节点不互相关联|boolean| -|true|
|readonly| 是否只读|boolean| -|false|
|show-all-levels| 输入框中是否显示选中值的完整路径|boolean| -|true|
|collapse-tags| 多选模式下是否折叠Tag|boolean| -|false|
|@change| 改变数值时候的回调|Function| -|默认会代入当前id|
---
### 4.HtSelectOrg--选择部门
>组件使用说明:主要用于在系统中需要选择部门或者查看部门时候使用
```c
 <ht-select-org v-model="value"></ht-select-org>
```
**Attributes/Events:**
|参数名|说明 | 类型 |可选值|默认值|
|--|--|--|--|--|
|value/v-model| 部门ID |string | -|-|
|org| 部门id,传入值之后就会根据该值自动过滤成该部门以及该部门的子部门|string | -|-|
|size| 尺寸|string |medium / small / mini-|-|
|placeholder| 输入框占位文本|string| -|-|
|disabled| 是否禁用|boolean| -|false|
|clearable| 是否可以清除|boolean| -|false|
|multiple| 是否可以多选|boolean| -|false|
|check-strictly| 是否严格的遵守父子节点不互相关联|boolean| -|true|
|readonly| 是否只读|boolean| -|false|
|show-all-levels| 输入框中是否显示选中值的完整路径|boolean| -|true|
|collapse-tags| 多选模式下是否折叠Tag|boolean| -|false|
|@change| 改变数值时候的回调|Function| -|默认会代入当前id|
---
### 4.HtSelectBaseData--选择基础数据
>组件使用说明:主要用于在系统中需要选择基础数据时候使用
```c
 <ht-select-base-data v-model="value"></ht-select-base-data>
```
**Attributes/Events:**
|参数名|说明 | 类型 |可选值|默认值|
|--|--|--|--|--|
|value/v-model| 部门ID |string | -|-|
|dataTypeId| 基础数据的Value值(必填)|string| 参考基础数据页面的Value|-|
|org| 部门id,传入值之后就会根据该值自动过滤成该部门下面的基础数据|string | -|-|
|size| 尺寸|string |medium / small / mini-|-|
|placeholder| 输入框占位文本|string| -|-|
|disabled| 是否禁用|boolean| -|false|
|clearable| 是否可以清除|boolean| -|false|
|multiple| 是否可以多选|boolean| -|false|
|check-strictly| 是否严格的遵守父子节点不互相关联|boolean| -|true|
|readonly| 是否只读|boolean| -|false|
|hide-code| 是否隐藏数据展示或选中时候的value值|boolean| -|false|
|show-all-levels| 输入框中是否显示选中值的完整路径|boolean| -|true|
|collapse-tags| 多选模式下是否折叠Tag|boolean| -|false|
|by-code| 是否通过基础数据的value值来做唯一key,当前默认是通过id来选择值.当选择严重等级或响应等级时候必须传入true|boolean| -|false|
|@change| 改变数值时候的回调|Function| -|默认会代入当前id|
---
### 5.HtTable--table表格
>组件使用说明:在系统中表格样式和功能统一封装(基于Element UI具体文档也可以参考[https://element.eleme.cn/#/zh-CN/component/table](https://element.eleme.cn/#/zh-CN/component/table)),该组件主要提供了分页功能,列的自定义展示功能,列的拖动排序功能
```c
 <ht-table
      :data="state.dataSource"
      @onchange="changePage"
      border
      height="calc(100vh - 200px)"
      highlight-current-row
      :page-info="{
        currentPage: state.currentPage,
        pageSize: state.maxResultCount,
        skipCount: state.skipCount,
        totalCount: state.totalCount,
      }"
      :columns="state.columns"
    >
      <template slot-scope="{ row }" slot="handel">
        <el-button type="text" style="margin-left:12px" @click="edit(row)"
          >编辑</el-button
        >
      </template>
    </ht-table>
```
**Table Attributes:**
|参数名|说明 | 类型 |可选值|默认值|
|--|--|--|--|--|
|data| 显示的数据|array| -|-|
|columns| 详情见Column属性|Column[]| -|-|
|height| Table 的高度，默认为自动高度。如果 height 为 number 类型，单位 px；如果 height 为 string 类型，则这个高度会设置为 Table 的 style.height 的值，Table 的高度会受控于外部样式。)|string/number| -|-|
|max-height|Table 的最大高度。合法的值为数字或者单位为 px 的高度。|string/number| -|-|
|stripe| 是否为斑马纹 table|boolean-|true|
|border| 是否带有纵向边框|boolean| -|true|
|fit| 列的宽度是否自撑开禁用|boolean| -|true|
|show-header| 是否显示表头|boolean| -|true|
|highlight-current-row| 是否要高亮当前行|boolean| -|true|
|current-row-key| 当前行的 key，只写属性|String,Number| -|-|
|row-class-name| 行的 className 的回调方法，也可以使用字符串为所有行设置一个固定的 className。|Function({row, rowIndex})/String| -|-|
|row-style|行的 style 的回调方法，也可以使用一个固定的 Object 为所有行设置一样的 Style。|Function({row, rowIndex})/Object| -|-|
|header-row-class-name| 表头行的 className 的回调方法，也可以使用字符串为所有表头行设置一个固定的 className|Function({row, rowIndex})/String| -|-|
|header-row-style| 表头行的 style 的回调方法，也可以使用一个固定的 Object 为所有表头行设置一样的 Style。|Function({row, rowIndex})/Object| -|{ background: 'var(--primary-92)' }|
|header-cell-class-name| 表头单元格的 className 的回调方法，也可以使用字符串为所有表头单元格设置一个固定的 className|Function({row, column, rowIndex, columnIndex})/String| -|-|
|header-cell-style| 表头单元格的 style 的回调方法，也可以使用一个固定的 Object 为所有表头单元格设置一样的 Style。|Function({row, column, rowIndex, columnIndex})/Object| -|-|
|row-key| 行数据的 Key，用来优化 Table 的渲染；在使用 reserve-selection 功能与显示树形数据时，该属性是必填的。类型为 String 时，支持多层访问：user.info.id，但不支持 user.info[0].id，此种情况请使用 Function。|Function(row)/String| -|id|
|empty-text| 空数据时显示的文本内容，也可以通过 slot="empty" 设置(开发中)|string| -|暂无数据|
|checked| 是否启用复选框|boolean| -|false|
|selectKey| 设置禁用check行的字段 ,通过该字段来判断改行是否禁用,所以data数据中也必须包含该字段|string| -|selectable|
|show-filter| 是否启用自定义列展示排序功能|boolean| -|false|
|draggable|是否可以拖动排序|boolean| -|true|
|config-show| 为true时候默认表头就为空,否则就为初始的全部,需配合show-filter一起使用|boolean| -|false|
|show-filter| 是否启用自定义列展示排序功能|boolean| -|false|
|uu-id| table的唯一key用于缓存自定义列功能,推荐使用UUID格式|string| -|-|
|hide-order| 是否隐藏序号那一列|boolean| -|false|
|key-name| 序号那列的头名称|string| -|序号|
|hide-page| 是否隐藏分页|boolean| -|false|
|page-info| 详情见pageInfo属性|PageInfoType| -|-|
|pagination| 分页的一些额外属性(如:hideOnSinglePage,small)|object| -|-|


**Column Attributes/Events:**
|参数名|说明 | 类型 |可选值|默认值|
|--|--|--|--|--|
|currentPage| 唯一KEY |string | -|-|
|title| 列名| string|-|-|
|width|列宽|string /number| -|-|
|minWidth| 最小宽度|string/number |-|-|
|fixed| 列是否固定在左侧或者右侧，true 表示固定在左侧|string, boolean| true, left, right|-|
|sortable| 对应列是否可以排序，如果设置为 'custom'，则代表用户希望远程排序，需要监听 Table 的 sort-change 事件|boolean, string| true, false, 'custom'|false|
|resizable| 对应列是否可以通过拖动改变宽度（需要在 el-table 上设置 border 属性为真）|boolean| -|true|
|align| 对齐方式|string| left/center/right|left|
|headerAlign| 表头对齐方式，若不设置该项，|string| left/center/right|left|
|className| 列的 className|string| -|-|
|labelClassName| 当前列标题的自定义类名|string| -|-|
|hide| 是否隐藏当前列|boolean| -|false|
|deafaultShow| 默认是否显示|boolean| -|-|
|spread| 时间是否跨行显示(上下结构的时分秒-年月日)|boolean| -|false|
|type| 数据自动解析时候的类型(人员ID,部门ID,时间,基础数据,布尔值,图片,附件)|userId /org/ time/ common/ boolean/ img / file| -|-|
|commonType| 只有当type='common'时候有效 |baseDataId /roleId/ baseDataName/baseDataValue| -|-|
|hideode| 当type===common时候 设置是否隐藏基础数据的value |boolean| -|-|
|showOverflowTooltip| 当内容过长被隐藏时显示 tooltip |boolean| -|true|
|hideCode| 当type===common时候 设置是否隐藏基础数据的value |boolean| -|-|
|disabled| 自定义列是否是否禁止操作该列 |boolean| -|-|
|property| 自定义列时候展示额外信息 |string| -|-|
|checked| 自定义列时候默认是否选中 |boolean| -|-|


**PageInfo Attributes/Events:**
|参数名|说明 | 类型 |可选值|默认值|
|--|--|--|--|--|
|currentPage| 当前第几页 |number| -|-|
|pageSize| 每页最大条数 |number| -|-|
|currentPage| 当前第几页 |number| -|-|
|skipCount| 跳过多少条开始查询 |number| -|-|

**Table Events:**
>主要的事件都可以参考Element UI官方 这里不在详细列出,只列出新增部分

|参数名|说明 | 参数|
|--|--|--|
|onchange| 分页回调 |(data:PageType)|
|customColumn| 自定义列的回调 |(columns:Column[ ])|

**Table Slot:**
|name|说明 | 
|--|--|
|${item.key}|自定义列的内容，参数为 { row, column, $index },例如:当要修改column中key=age的内容,slot中name=age| 
|header_${item.key}|处理重定义table头相关信息 header_key,参数为{ {column, $index }例如:当要修改column中key=age的头部内容,slot中name=header_age| 
|footerLeft|自定义分页内容,当hidePage=true时候将失效,此处建议使用el-col| 
|header_order|排序列的头信息,自定义之后自定义排序功能失效,默认显示序号| 
|body_order|排序列的body信息,默认展示的是当前行所在的位置,参数为 { row, column, $index }| 

# 其他


如果发现什么bug可以在评论区留言,正在不断完善中
