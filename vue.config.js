/*
 * @Descripttion: 
 * @version: 
 * @Author: hutao
 * @Date: 2021-11-15 14:41:40
 * @LastEditors: hutao
 * @LastEditTime: 2024-08-03 09:59:21
 */
const CompressionPlugin = require("compression-webpack-plugin");
const baseConfig = require("./public/baseConfig.json");
const Timestamp = new Date().getTime(); //当前时间为了防止打包缓存不刷新，所以给每个js文件都加一个时间戳
module.exports = {
  publicPath: baseConfig.baseUrl,
  productionSourceMap: process.env.NODE_ENV === "production" ? false : true,
  devServer: {
    proxy: 'http://192.168.3.246'
    //proxy: {
    // 公共数据
    // "^/real_time": {
    //   target: "http://192.168.3.246", // 设置调用接口的域名和端口号
    //   // ws: true, // 是否启用websocket代理
    //   changeOrigin: true, // 是否跨域
    //   pathRewrite: {}
    // },
    // "^/auth": {
    //   target: "http://192.168.3.246", // 设置调用接口的域名和端口号
    //   // ws: true, // 是否启用websocket代理
    //   changeOrigin: true, // 是否跨域
    //   pathRewrite: {}
    // },
    // "^/cctv": {
    //   target: "http://192.168.3.246", // 设置调用接口的域名和端口号
    //   // ws: true, // 是否启用websocket代理
    //   changeOrigin: true, // 是否跨域
    //   pathRewrite: {}
    // },
    // "^/setting-management-service": {
    //   target: "http://192.168.3.246", // 设置调用接口的域名和端口号
    //   // ws: true, // 是否启用websocket代理
    //   changeOrigin: true, // 是否跨域
    //   pathRewrite: {}
    // },
    // "^/hd-authserver": {
    //   target: "http://192.168.3.246", // 设置调用接口的域名和端口号
    //   // ws: true, // 是否启用websocket代理
    //   changeOrigin: true, // 是否跨域
    //   pathRewrite: {}
    // },
    // "^/menu-management-service": {
    //   target: "http://192.168.3.246", // 设置调用接口的域名和端口号
    //   // ws: true, // 是否启用websocket代理
    //   changeOrigin: true, // 是否跨域
    //   pathRewrite: {}
    // },
    // // 授权
    // "^/oauth2": {
    //   target: "http://192.168.3.246", // 设置调用接口的域名和端口号
    //   // ws: true, // 是否启用websocket代理
    //   changeOrigin: true, // 是否跨域
    //   pathRewrite: {}
    // },
    // // 授权组件
    // "^/signalr-dataopening": {
    //   target: "http://192.168.3.246", // 设置调用接口的域名和端口号
    //   ws: true, // 是否启用websocket代理
    //   changeOrigin: true, // 是否跨域
    //   pathRewrite: {}
    // }
    // }
  },
  configureWebpack: config => {
    if (process.env.NODE_ENV === "production") {
      return {
        output: {
          // 输出重构  打包编译后的 文件名称  【模块名称.时间戳】
          filename: `js/[name].${Timestamp}.js`,
          chunkFilename: `js/[name].${Timestamp}.js`
        },
        plugins: [
          new CompressionPlugin({
            test: /\.js$|\.html$|\.css/, // 匹配文件名
            threshold: 10240, // 对超过10K的数据进行压缩
            deleteOriginalAssets: false // 是否删除源文件
          })
        ]
      };
    }
  }
};
