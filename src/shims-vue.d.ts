/*
 * @Descripttion: 
 * @version: 
 * @Author: hutao
 * @Date: 2021-11-15 14:41:40
 * @LastEditors: hutao
 * @LastEditTime: 2021-12-29 10:23:59
 */
declare module "*.vue" {
  import Vue from "vue";
  export default Vue;
}
declare module "*.js" {
  const body: any

  export default body;
}
declare module "*.png"
