/*
 * @Descripttion: 
 * @version: 
 * @Author: hutao
 * @Date: 2021-12-29 15:18:26
 * @LastEditors: hutao
 * @LastEditTime: 2023-04-09 11:05:05
 */
import { RouteConfig } from "vue-router";
import Index from "../views/Index.vue";
import Login from "vue-kst-auth";
import About from "../views/About.vue";

const routes: Array<RouteConfig> = [
  {
    path: "/login",
    name: "登录",
    component: Login as any
  },

  {
    path: "/",
    name: "首页",
    component: Index,
    children: [
      {
        path: "",
        name: "",
        meta: {
          title: "资产管理"
        },
        component: About
      },
      {
        path: "assetManagement",
        name: "",
        meta: {
          title: "资产管理"
        },
        component: Index
      },
      {
        path: "fireManagement",
        name: "",
        meta: {
          title: "消防管理"
        },
        component: Index
      },
      {
        path: "laboratoryManagement",
        name: "",
        meta: {
          title: "化验室管理"
        },
        component: Index
      },
      {
        path: "extendProperty",
        name: "",
        meta: {
          title: "扩展属性管理"
        },
        component: Index
      },

      {
        path: "extendPropertyTemplate",
        name: "",
        meta: {
          title: "扩展属性模板管理"
        },
        component: Index
      },
      {
        path: "assetCategory",
        name: "",
        meta: {
          title: "资产分类"
        },
        component: Index
      },
      {
        path: "assetAdscription",
        name: "",
        meta: {
          title: "资产类型"
        },
        component: Index
      },
      {
        path: "produceLevel",
        name: "",
        meta: {
          title: "节点层次"
        },
        component: Index
      },
      {
        path: "/operationalModel",
        name: "运维模型",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: Index
      },
      {
        path: "/bindoperationalModel",
        name: "绑定运维模型",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: Index
      },
      {
        path: "/operationalDevices",
        name: "运维设备",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: Index
      },
      {
        path: "/assetRetire",
        name: "资产报废管理",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: Index
      },
      {
        path: "/httpAction",
        name: "资产第三方接口管理",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: Index
      },
      {
        path: "/Sop",
        name: "SOP维护",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: Index
      }
    ]
  },
]

// const router = new VueRouter({
//   routes
// });

export default routes;
