export default {
  state: {
    // "企业名称",
    enterpriseName: "星环视界智能科技",
    // "基本请求路径",
    baseUrl: "",
    /** 授权 */
    grantedPolicies: {},
    /** 部门人员 */
    organizationUsers: [],
    /** 部门 */
    organizationUnit: [],
    /** 人员 */
    users: [],
    /** 严重等级 */
    severityLevel: [],
    /** 资产分类 */
    assetTypes: [],
    /** 位置 */
    positions: [],
    /** 分组 */
    grids: []
  },
  getters: {
    getBaseData: (state: any) => (name: string) => state[name]
  },
  mutations: {
    setBaseData(state: any, baseData: any) {
      for (const key in baseData) {
        if (baseData.hasOwnProperty(key) && !/(^\?)|(^？)/.test(key)) {
          let element: string = baseData[key];
          if (/^baseUrl/.test(key)) {
            element = element.replace(/\/$/g, "");
          }
          state[key] = element;
        }
      }
    },
    setBaseDataApi(state: any, payload: { key: string; value: any }) {
      state[payload.key] = payload.value;
    }
  },
  actions: {}
};
