import Vue from "vue";
import Vuex from "vuex";
import baseData from "./baseData"; // 基本数据

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    baseData
  }
});
