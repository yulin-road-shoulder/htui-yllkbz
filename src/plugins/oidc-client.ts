/*
 * @Descripttion:
 * @version:
 * @Author: hutao
 * @Date: 2022-05-06 17:56:08
 * @LastEditors: hutao
 * @LastEditTime: 2023-06-07 17:10:52
 */
// /* eslint-disable @typescript-eslint/camelcase */
// /*
//  * @Descripttion:
//  * @version:
//  * @Author: hutao
//  * @Date: 2022-05-06 17:56:08
//  * @LastEditors: hutao
//  * @LastEditTime: 2023-04-09 16:02:30
//  */
// import Oidc from "oidc-client";
// function getCurrentUrl() {
//   let href = window.location.pathname;
//   if (href.charAt(href.length - 1) === '/') {
//     href = href.substring(0, href.length - 1)
//   }
//   //console.log('href', href, window.location);
//   ///console.log('window.location.host + href', window.location.origin + href);
//   return window.location.origin + href
// }
// export const mgr = new Oidc.UserManager({
//   // authority: "oauth2/api",//认证服务器
//   authority: "http://192.168.3.237:44318",//认证服务器
//   client_id: "AuthServer_App", //表示客户端的ID，必选项
//   //redirect_uri: "http://192.168.3.113:8082/product_architecture", //表示重定向URI，认证服务器回调的客户端页面。可选项
//   redirect_uri: getCurrentUrl(), //表示重定向URI，认证服务器回调的客户端页面。可选项
//   // redirect_uri: "http://192.168.3.252:4200", //表示重定向URI，认证服务器回调的客户端页面。可选项
//   response_type: "code", // response_type：表示授权类型，必选项
//   userStore: new Oidc.WebStorageStateStore({ store: window.localStorage }),
//   scope: "offline_access AuthServer", //scope：表示申请的权限范围，可选项
//   post_logout_redirect_uri: getCurrentUrl(),
//   automaticSilentRenew: false,

//   extraQueryParams: {
//     _tenant: 'default'
//   }

// });
