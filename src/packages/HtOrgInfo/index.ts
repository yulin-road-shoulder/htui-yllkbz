/*
 * @Descripttion:选择部门
 * @version:
 * @Author: hutao
 * @Date: 2021-11-15 15:00:57
 * @LastEditors: hutao
 * @LastEditTime: 2022-04-12 17:59:54
 */
import HtOrgInfo from "./index.vue";
(HtOrgInfo as any).install = function (Vue: any) {

  Vue.component("HtOrgInfo", HtOrgInfo);
};
export default HtOrgInfo;