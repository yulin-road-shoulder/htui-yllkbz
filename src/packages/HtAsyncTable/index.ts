/*
 * @Descripttion:
 * @version:
 * @Author: hutao
 * @Date: 2021-11-15 15:00:57
 * @LastEditors: hutao
 * @LastEditTime: 2021-12-09 14:21:20
 */
import HtTable from "./index.vue";

(HtTable as any).install = function (Vue: any) {

  Vue.component("HtTable", HtTable);
};

export default HtTable;