/*
 * @Descripttion:
 * @version:
 * @Author: hutao
 * @Date: 2021-11-15 15:00:57
 * @LastEditors: hutao
 * @LastEditTime: 2022-03-08 14:41:55
 */
import HtSelectBaseData from "./index.vue";
(HtSelectBaseData as any).install = function (Vue: any) {

  Vue.component("HtSelectBaseData", HtSelectBaseData);
};
export default HtSelectBaseData;