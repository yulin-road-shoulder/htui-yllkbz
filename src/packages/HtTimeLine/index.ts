/*
 * @Descripttion:选择资产单位
 * @version:
 * @Author: hutao
 * @Date: 2021-11-15 15:00:57
 * @LastEditors: hutao
 * @LastEditTime: 2023-06-01 17:24:53
 */
import HtTimeLine from "./index.vue";
(HtTimeLine as any).install = function (Vue: any) {

  Vue.component("HtTimeLine", HtTimeLine);
};
export default HtTimeLine;