/*
 * @Descripttion:cron表达式选择
 * @version:
 * @Author: hutao
 * @Date: 2021-11-15 15:00:57
 * @LastEditors: hutao
 * @LastEditTime: 2023-01-03 14:15:44
 */
import HtSelectCron from "./index.vue";
(HtSelectCron as any).install = function (Vue: any) {

  Vue.component("HtSelectCron", HtSelectCron);
};
export default HtSelectCron;