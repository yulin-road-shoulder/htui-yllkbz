/*
 * @Descripttion: 基础数据展示组件
 * @version: 
 * @Author: hutao
 * @Date: 2022-04-12 17:34:51
 * @LastEditors: hutao
 * @LastEditTime: 2023-04-21 16:31:50
 */

import HtRow from "./index.vue";
(HtRow as any).install = function (Vue: any) {

  Vue.component("HtRow", HtRow);
};
export default HtRow;