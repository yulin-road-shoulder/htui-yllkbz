/*
 * @Descripttion: 
 * @version: 
 * @Author: hutao
 * @Date: 2021-10-21 10:08:41
 * @LastEditors: hutao
 * @LastEditTime: 2025-03-12 17:07:44
 */

// 导入组件
import { VueConstructor } from 'vue'
/** 下拉table选择控件 */
import HtSelectTable from './SelectTable/index'
/** 分页组装配件 */
import HtPagination from './PageInfo/index'
import HtTable from './HtTable/index'
import HtExport from './HtExport/index'
import HtUpload from './HtUpload/index'
import HtMd from './HtMd/index'
import HtCountDown from './HtCountDown/index'
import HtUploadFiles from './HtUploadFiles/index'
import HtSelectBaseData from './HtSelectBaseData/index'
import HtSelectOrg from './HtSelectOrg/index'
import HtSelectUser from './HtSelectUser/index'
import HtShowBaseData from './HtShowBaseData/index'
import HtOrgInfo from './HtOrgInfo/index'
import HtBaseData from './HtBaseData/index'
import HtShowBaseType from './HtShowBaseType'
import HtDrawer from './HtDrawer'
import HtSelectCron from './HtSelectCron'
import HtSelectTimeSlot from './HtSelectTimeSlot'
import HtMore from './HtMore'
import HtSelectUnit from './HtSelectUnit'
import HtSelectPosition from './HtSelectPosition'
import HtSelectCategory from './HtSelectCategory'
import HtMenu from './HtMenu'
import HtRow from './HtRow'
import HtCol from './HtCol'
import HtModel from "./HtModel"
import HtDialog from "./HtDialog"
import HtBread from './HtBread'
import HtPopover from './HtPopover'
import HtTimeLine from './HtTimeLine'
import HtTimeLineItem from './HtTimeLineItem'
import HtRealTime from './HtRealTime'
// import HtOffice from './HtOffice'






// 存储组件列表
const components = [HtRealTime, HtDialog,HtRow, HtTimeLineItem, HtCol, HtTimeLine, HtBread, HtPopover, HtModel, HtMenu, HtSelectCategory, HtSelectUnit, HtSelectPosition, HtMore, HtSelectTimeSlot, HtSelectCron, HtBaseData, HtDrawer, HtShowBaseType, HtSelectTable, HtPagination, HtTable, HtExport, HtUpload, HtMd, HtCountDown, HtUploadFiles, HtSelectBaseData, HtSelectOrg, HtSelectUser, HtShowBaseData, HtOrgInfo]
// 定义 install 方法，接收 Vue 作为参数。如果使用 use 注册插件，则所有的组件都将被注册
const install = function (Vue: any) {
  // 判断是否安装
  if ((install as any).installed) return
  // 遍历注册全局组件
  components.map((component: any, index) => Vue.component(component.options.name || component.name, component))
}
// 判断是否是直接引入文件
if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue)
}
export default {
  // 导出的对象必须具有 install，才能被 Vue.use() 方法安装
  install,
  // 以下是具体的组件列表
  HtRow, HtCol, HtRealTime, HtModel, HtBread, HtPopover,
  HtSelectTable, HtSelectPosition, HtPagination, HtShowBaseType, HtTable, HtExport, HtUpload, HtMd, HtCountDown, HtUploadFiles, HtMore,
  HtSelectUnit, HtSelectCategory, HtMenu,
  HtSelectBaseData, HtSelectOrg,HtDialog, HtTimeLine, HtTimeLineItem, HtSelectUser, HtShowBaseData, HtOrgInfo, HtBaseData, HtDrawer, HtSelectCron, HtSelectTimeSlot
}

