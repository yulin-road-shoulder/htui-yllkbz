/*
 * @Descripttion:选择部门
 * @version:
 * @Author: hutao
 * @Date: 2021-11-15 15:00:57
 * @LastEditors: hutao
 * @LastEditTime: 2022-03-08 14:54:22
 */
import HtSelectUser from "./index.vue";
(HtSelectUser as any).install = function (Vue: any) {

  Vue.component("HtSelectUser", HtSelectUser);
};
export default HtSelectUser;