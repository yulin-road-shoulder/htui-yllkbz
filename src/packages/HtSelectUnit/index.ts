/*
 * @Descripttion:选择资产单位
 * @version:
 * @Author: hutao
 * @Date: 2021-11-15 15:00:57
 * @LastEditors: hutao
 * @LastEditTime: 2023-01-12 16:58:07
 */
import HtSelectUnit from "./index.vue";
(HtSelectUnit as any).install = function (Vue: any) {

  Vue.component("HtSelectUnit", HtSelectUnit);
};
export default HtSelectUnit;