/*
 * @Descripttion:
 * @version:
 * @Author: hutao
 * @Date: 2021-11-15 15:00:57
 * @LastEditors: hutao
 * @LastEditTime: 2021-11-30 14:18:58
 */
import HtSelectTable from "./index.vue";

(HtSelectTable as any).install = function (Vue: any) {

  Vue.component("HtSelectTable", HtSelectTable);
};

export default HtSelectTable;