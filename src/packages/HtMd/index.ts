/*
 * @Descripttion:markdown 编辑器
 * @version:
 * @Author: hutao
 * @Date: 2021-11-15 15:00:57
 * @LastEditors: hutao
 * @LastEditTime: 2021-12-21 16:24:26
 */
import HtMd from "./index.vue";
(HtMd as any).install = function (Vue: any) {
  Vue.component("HtMd", HtMd);
};
export default HtMd;