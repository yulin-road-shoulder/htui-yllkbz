/*
 * @Descripttion:选择资产单位
 * @version:
 * @Author: hutao
 * @Date: 2021-11-15 15:00:57
 * @LastEditors: hutao
 * @LastEditTime: 2023-06-02 09:26:55
 */
import HtTimeLineItem from "./index.vue";
(HtTimeLineItem as any).install = function (Vue: any) {

  Vue.component("HtTimeLineItem", HtTimeLineItem);
};
export default HtTimeLineItem;