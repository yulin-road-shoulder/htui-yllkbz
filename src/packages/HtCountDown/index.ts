/*
 * @Descripttion简易版的倒计时
 * @version:
 * @Author: hutao
 * @Date: 2021-11-15 15:00:57
 * @LastEditors: hutao
 * @LastEditTime: 2021-12-26 11:20:45
 */
import HtCountDown from "./index.vue";
(HtCountDown as any).install = function (Vue: any) {
  Vue.component("HtCountDown", HtCountDown);
};
export default HtCountDown;