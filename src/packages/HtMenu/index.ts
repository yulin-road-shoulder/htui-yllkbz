/*
 * @Descripttion: 更多三个点功能
 * @version: 
 * @Author: hutao
 * @Date: 2022-04-12 17:34:51
 * @LastEditors: hutao
 * @LastEditTime: 2023-03-21 10:50:04
 */

import HtMenu from "./index.vue";
(HtMenu as any).install = function (Vue: any) {

  Vue.component("HtMenu", HtMenu);
};
export default HtMenu;