/*
 * @Descripttion:选择部门
 * @version:
 * @Author: hutao
 * @Date: 2021-11-15 15:00:57
 * @LastEditors: hutao
 * @LastEditTime: 2022-03-08 14:50:29
 */
import HtSelectOrg from "./index.vue";
(HtSelectOrg as any).install = function (Vue: any) {

  Vue.component("HtSelectOrg", HtSelectOrg);
};
export default HtSelectOrg;