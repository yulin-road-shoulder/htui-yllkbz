/*
 * @Descripttion:
 * @version:
 * @Author: hutao
 * @Date: 2021-10-25 17:05:17
 * @LastEditors: hutao
 * @LastEditTime: 2023-04-14 10:35:56
 */
/** 初始的默认条数 */
export const defalutPageSize = 10
/** 分页显示传入的参数 
 * @param currentPage(当前页码)
 * @param pageSize(每页条数)
 * @param total(总条数)
*/
export interface CurrentType {
  currentPage: number;
  pageSize: number;
  total: number;
}
export interface ConfigProp {
  /** 展示的相关列表code */
  key: string;
  /** 是否可用 */
  disabled: boolean;
  /** 清除内容 */
  clearable?: boolean;
  /** 默认值 */
  value?: string; //code:"344"
  /** value对应的匹配值 */
  name?: string;
  ajax: {
    /** 请求地址 */
    url: string;
    /** 请求类型 */
    type?: "get" | "post";
    /** 头部参数 */
    params: any;
    /** body参数 */
    data?: any;
  };
  /** 展示的内容 */
  text?: string | number;
  table?: {
    rowkey?: string;
    height?: number;
    columns: Column[] | undefined;
  };

}
/**  参考element Table https://element.eleme.cn/#/zh-CN/component/table */
export interface Column {

  key: string;
  title: string;

  width?: number | string;
  minWidth?: number | string;
  fixed?: 'left' | 'right' | boolean;
  sortable?: 'custom' | boolean;
  resizable?: boolean;
  align?: 'left' | 'right' | 'center';
  headerAlign?: 'left' | 'right' | 'center';
  className?: string;
  labelClassName?: string;
  style?: string;
  /** 是否隐藏当前列 */
  hide?: boolean;
  /**  时间是否跨行展示 */
  spread?: boolean;
  /** 通过type展示相应的数据  用户id|部门id|时间格式化|是否布尔值|图片 |附件 |* |unit是新资产的单位  position资产位置 |日期 */
  type?: 'userId' | 'org' | 'time' | 'common' | 'boolean' | 'img' | 'file' | 'unit' | 'position' | 'date';
  /**    只有当type='common'时候有效  数据类型个ca common里面的一样但不包括时间 时间使用time  */
  commonType?: 'userId' | 'departmentId' | 'baseDataId' | 'roleId' | 'baseDataName' | 'baseDataValue';
  /** 当type===common时候 设置是否隐藏基础数据的value */
  hideCode?: boolean;
  showOverflowTooltip?: boolean;
  /** 默认是否展示 */
  deafaultShow?: boolean;
  /** 筛选时候是否禁用 */
  disabled?: boolean;
  /** 自定义列时候展示额外信息 */
  property?: string;
  /** 自定义列时候处理是否被选中 */
  checked?: boolean;
  /** 是否存在合并单元格,如果有子项则用children处理 */
  children?: Column[];
}
export interface PageInfoType {
  currentPage: number;
  pageSize: number;
  skipCount: number;
  totalCount: number;
}
export interface MenuDto {
  id?: string;
  creationTime?: string;
  creatorId?: string | undefined;
  lastModificationTime?: string | undefined;
  lastModifierId?: string | undefined;
  isDeleted?: boolean;
  deleterId?: string | undefined;
  deletionTime?: string | undefined;
  name?: string | undefined;
  displayName?: string | undefined;
  features?: string | undefined;
  path?: string | undefined;
  order?: number;
  description?: string | undefined;
  icon?: string | undefined;
  position?: number;
  isEnabled?: boolean;
  target?: string | undefined;
  code?: string | undefined;
  level?: number;
  parentId?: string | undefined;
  children?: MenuDto[] | undefined;
  childrenCount?: number;
}
/** 分页返回时候的分页信息 */
export interface PageType {

  currentPage: number;
  maxResultCount: number;
  skipCount: number;
  totalCount: number;

}
/** 二次封装面包屑的类型 */
export interface BreadType {
  /** 唯一值 */
  key: string;
  /** 名称 */
  title: string;
  /** vue的路由跳转 */
  to?: string;
  /** 完成的路由跳转 */
  url?: string;
  style?: string;
  /** 跳转方式针对url */
  target?: string;
}
/** cron表达式相关类型 */
export enum TimeModes {
  Minute = 'Minute',
  Hour = 'Hour',
  Day = 'Day',
  Week = 'Week',
  Month = 'Month',
  Year = 'Year',
}
export interface FormValues {

  periodUnit?: TimeModes;
  month?: number;
  day?: number;
  hour?: number;
  minute?: number;
  weekDay?: number;
  cronExpression?: string;

}
/**  附件相关配置 */
export interface InFile {
  fileName: string;
  fileSize: number;
  fileToken: string;
  fileType: string;
  id: string;
}
export interface UploadType {
  name: string;
  percentage: number;
  raw: {
    lastModified: number;
    lastModifiedDate: Date;
    name: string;
    size: number;
    type: string;
    uid: number;
    webkitRelativePath: string;
  };
  response: { fileToken: string };
  size: number;
  status: string;
  uid: number;
  id?: string;
  url: string;
}
