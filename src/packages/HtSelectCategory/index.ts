/*
 * @Descripttion:选择资产单位
 * @version:
 * @Author: hutao
 * @Date: 2021-11-15 15:00:57
 * @LastEditors: hutao
 * @LastEditTime: 2023-01-15 14:45:42
 */
import HtSelectCategory from "./index.vue";
(HtSelectCategory as any).install = function (Vue: any) {

  Vue.component("HtSelectCategory", HtSelectCategory);
};
export default HtSelectCategory;