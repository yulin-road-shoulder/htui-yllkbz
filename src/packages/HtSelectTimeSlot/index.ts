/*
 * @Descripttion:选择时间段
 * @version:
 * @Author: hutao
 * @Date: 2021-11-15 15:00:57
 * @LastEditors: hutao
 * @LastEditTime: 2023-01-03 14:39:35
 */
import HtSelectTimeSlot from "./index.vue";
(HtSelectTimeSlot as any).install = function (Vue: any) {

  Vue.component("HtSelectTimeSlot", HtSelectTimeSlot);
};
export default HtSelectTimeSlot;