/*
 * @Descripttion:
 * @version:
 * @Author: hutao
 * @Date: 2021-11-15 15:00:57
 * @LastEditors: hutao
 * @LastEditTime: 2021-12-08 11:39:52
 */
import HtPagination from "./index.vue";

(HtPagination as any).install = function (Vue: any) {

  Vue.component("HtPagination", HtPagination);
};

export default HtPagination;