/*
 * @Descripttion:树结构展示
 * @version:
 * @Author: hutao
 * @Date: 2021-11-15 15:00:57
 * @LastEditors: hutao
 * @LastEditTime: 2023-03-08 09:43:23
 */
import HtTree from "./index.vue";
(HtTree as any).install = function (Vue: any) {

  Vue.component("HtTree", HtTree);
};
export default HtTree;