/*
 * @Descripttion: 
 * @version: 
 * @Author: hutao
 * @Date: 2022-07-18 15:01:02
 * @LastEditors: hutao
 * @LastEditTime: 2024-09-24 09:01:39
 */
import moment from "moment";
import { FormValues, TimeModes } from "./type";
import { baseConfig } from "vue-kst-auth";

/**    生成唯一Id
 * @params e 生成的id位数 默认32
 */
export const randomString = (e = 32) => {
  /** 生成格式17位的时间YYYY-MM-DD HH:mm:ss.SSS加(e-17)位的随机数 */
  const t = "ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz23456789",
    a = t.length;
  const now = moment().format('YYYYMMDDHHmmssSSS')
  let n = "";
  if (e > 17) {
    n = now

    for (let i = 0; i < (e - 17); i++) {
      n += t.charAt(Math.floor(Math.random() * a));
    }
  }
  else if (e === 17) {
    n = now
  }
  else {
    for (let i = 0; i < e; i++) {
      n += t.charAt(Math.floor(Math.random() * a));
    }
  }

  return n;
}
/** 时间计算
@params fromDate 开始时间
@params toDate 结束时间
 */
export const dateLess = (fromDate: string, toDate: string) => {
  if (fromDate && toDate) {
    const date = new Date(fromDate);
    const date2 = new Date(toDate);
    const s1: number = date.getTime(),
      s2 = date2.getTime();
    const total: number = (s2 - s1) / 1000;
    const day: number = parseInt((total / (24 * 60 * 60)).toString()); //计算整数天数
    const afterDay: number = total - day * 24 * 60 * 60; //取得算出天数后剩余的秒数
    const hour: number = parseInt((afterDay / (60 * 60)).toString()); //计算整数小时数
    const afterHour = total - day * 24 * 60 * 60 - hour * 60 * 60; //取得算出小时数后剩余的秒数
    const min: number = parseInt((afterHour / 60).toString()); //计算整数分
    const afterMin: number =
      parseInt((total - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60).toString()); //取得算出分后剩余的秒数
    if (day === 0) {
      if (hour === 0) {
        return min + "分" + afterMin + "秒";
      } else {
        return hour + "小时" + min + "分" + afterMin + "秒";
      }
    } else {
      return day + "天" + hour + "小时" + min + "分" + afterMin + "秒";
    }
  }
  else {
    return "-"
  }

}
/** 根据id获取基础信息中名称
 * @parmas id 基础数据id
 * @parmas showobj 是否返回单个对象 否则返回name
 */
export function getCommDataName(id?: string, showobj = false) {
  if (!id) {
    return undefined
  }
  /** 基础数据 */
  const commonDatas = window.localStorage.getItem("commonDatas") || "";
  if (commonDatas) {
    const baseData = JSON.parse(commonDatas).baseData.items;
    const data = baseData.find((item: any) => item.id === id)
    if (showobj) {
      return data
    }
    else {
      return data.name
    }
  }
  return undefined;
}
/** 获取基础数据的相关内容
 * @parmas type 想要查询的类型 
 * @params keys 查询的内容
 */
export function getCommDataItem(type: "users" | "organizationUnit" | "dictionaryCategory" | "dictionaryData" | 'user' | 'org' | "baseData", keys?: string | string[]) {
  const commen = window.localStorage.getItem("commonDatas");
  if (!commen) {
    return undefined
  }
  const commonDatas = JSON.parse(commen)

  switch (type) {
    case "users"://返回所有人员
      return commonDatas.users.items;
    case "organizationUnit"://返回所有部门
      return commonDatas.organizationUnit;
    case "dictionaryData"://返回所有dictionaryData
      return commonDatas.dictionaryData;
    case "dictionaryCategory":
      return commonDatas.dictionaryCategory.items;
    case "user"://查询所有的关联的人员信息
      const users = commonDatas.users.items;
      if (keys) {
        if (Array.isArray(keys)) {
          return users.filter((item: any) => keys.toString().includes(item.id))
        }
        else {
          return users.find((item: any) => item.id === keys)
        }
      }
      else {
        return undefined;
      }
    case "org"://查询所有的关联的部门信息
      const organizationUnit = commonDatas.organizationUnit;
      if (keys) {
        if (Array.isArray(keys)) {
          return organizationUnit.filter((item: any) => keys.toString().includes(item.id))
        }
        else {
          return organizationUnit.find((item: any) => item.id === keys)
        }
      }
      else {
        return undefined;
      }
    case "baseData"://查询所有的id关联的基础数据
      const baseData = commonDatas.baseData.items;
      if (keys) {
        if (Array.isArray(keys)) {
          return baseData.filter((item: any) => keys.toString().includes(item.id))
        }
        else {
          return baseData.find((item: any) => item.id === keys)
        }
      }
      else {
        return baseData
      }
  }




}


export const getCronExpressionByPartition = ({
  minute,
  month,
  hour,
  weekDay,
  periodUnit,
  day,
}: FormValues) => {
  switch (periodUnit as TimeModes) {
    case TimeModes.Minute:
      return `0 */${minute || 0} * * * ?`;
    case TimeModes.Hour:
      return `0 ${minute || 0} * * * ?`;
    case TimeModes.Day:
      return `0 ${minute || 0} ${hour || 0} * * ?`;
    case TimeModes.Week:
      return `0 ${minute || 0} ${hour || 0} ? * ${weekDay || 0}`;
    case TimeModes.Month:
      return `0 ${minute || 0} ${hour || 0} ${day || 0} * ?`;
    case TimeModes.Year:
      return `0 ${minute || 0} ${hour || 0} ${day || 0} ${month || 0} ?`;
    default:
      return '0 */10 * * * ?';
  }
};

const computePeriodUnit = (cronExpression: string) => {
  const partitions = cronExpression.split(' ');
  const stars = partitions.filter(item => item === '*').length;
  switch (stars) {
    case 3:
      return partitions[1].includes('/') ? TimeModes.Minute : TimeModes.Hour;
    case 2:
      return TimeModes.Day;
    case 1:
      return partitions[partitions.length - 1] === '?'
        ? TimeModes.Month
        : TimeModes.Week;
    case 0:
      return TimeModes.Year;
    default:
      return TimeModes.Minute;
  }
};
export const getTimeValues = (cronExpression: string) => {
  const partitions = cronExpression.split(' ');
  const currentPeriodUnit = computePeriodUnit(cronExpression);
  let minute = +((partitions[1] || ([] as string[])).includes('/')
    ? partitions[1].slice(2) // slice(2) to remove */
    : partitions[1]);
  // min minute duration is 10
  if (currentPeriodUnit === 'Minute' && minute < 10) {
    minute = 10;
  }
  const hour = +partitions[2] || 0;
  const day = +partitions[3] || 1;
  const month = +partitions[4] || 1;
  const weekDay = +partitions[5] || 1;
  return { minute, hour, day, month, weekDay, periodUnit: currentPeriodUnit };
};
/** 四舍六入五成双 */
export const roundHalfToEven = (num: number, precision = 2) => {
  // 将数字乘以10的precision次方，使其小数点后只剩下需要保留的位数  
  const factor = Math.pow(10, precision);
  let tempNum = num * factor;

  // 判断是否需要进位  
  const digit = Math.round(tempNum) % 10;
  if (digit >= 5 && digit <= 9) {
    tempNum = Math.ceil(tempNum); // 进位  
  } else if (digit === 5) {
    // 检查后一位数字来决定是否进位  
    const nextDigit = Math.floor((tempNum * 10) % 10);
    if (nextDigit > 0) {
      tempNum = Math.ceil(tempNum); // 进位  
    }
  } else {
    tempNum = Math.floor(tempNum); // 直接舍去  
  }

  // 将结果除以10的precision次方，得到最终舍入后的结果  
  return tempNum / factor;
}

/** 获取所有部门信息 */
export const getAllOrg = () => {
  const baseDataItem: any = {}
  let resData = {
    baseData: {
      items: [],
    },
    dictionaryCategory: {
      items: [],
    },
    dictionaryData: [],
    severityLevel: [],
    organizationUnit: [],
    organizationUsersTree: [],
    users: {
      items: [],
    },
    SessionState: "",
    userInOrganiza: [],
    organizationUsers: {},
    roleList: [],
  }
  if (!baseConfig.getLoginState()) {
    return;
  }
  let data = window.localStorage.getItem("commonDatas");
  if (data) {
    resData = Object.assign(resData, JSON.parse(data));
    data = null;
  }
  const organizationUnit = resData.organizationUnit;
  /** 递归处理基础数据 */
  function getBaseDataItem(list: any[]) {
    list.forEach((item) => {
      baseDataItem[item.id || ""] = item;
      if (item.children) {
        getBaseDataItem(item.children);
      }
    });
  }
  getBaseDataItem(organizationUnit);
  return baseDataItem
}
/** 获取所有角色信息 */
export const getAllRole = () => {
  const baseDataItem: any = {}
  let resData = {
    baseData: {
      items: [],
    },
    dictionaryCategory: {
      items: [],
    },
    dictionaryData: [],
    severityLevel: [],
    organizationUnit: [],
    organizationUsersTree: [],
    users: {
      items: [],
    },
    SessionState: "",
    userInOrganiza: [],
    organizationUsers: {},
    roleList: [],
  }
  if (!baseConfig.getLoginState()) {
    return;
  }
  let data = window.localStorage.getItem("commonDatas");
  if (data) {
    resData = Object.assign(resData, JSON.parse(data));
    data = null;
  }
  const roleList = resData.roleList;
  /** 递归处理基础数据 */
  function getBaseDataItem(list: any[]) {
    list.forEach((item) => {
      baseDataItem[item.id || ""] = item;
      if (item.children) {
        getBaseDataItem(item.children);
      }
    });
  }
  getBaseDataItem(roleList);
  return baseDataItem
}
/** 获取所有用户信息 */
export const getAllUser = () => {
  const baseDataItem: any = {}
  let resData = {
    baseData: {
      items: [],
    },
    dictionaryCategory: {
      items: [],
    },
    dictionaryData: [],
    severityLevel: [],
    organizationUnit: [],
    organizationUsersTree: [],
    users: {
      items: [],
    },
    SessionState: "",
    userInOrganiza: [],
    organizationUsers: {},
    roleList: [],
  }
  if (!baseConfig.getLoginState()) {
    return;
  }
  let data = window.localStorage.getItem("commonDatas");
  if (data) {
    resData = Object.assign(resData, JSON.parse(data));
    data = null;
  }
  const { items = [] } = resData.users;

  /** 递归处理基础数据 */
  function getBaseDataItem(list: any[]) {
    list.forEach((item) => {
      baseDataItem[item.id || ""] = item;
      if (item.children) {
        getBaseDataItem(item.children);
      }
    });
  }
  getBaseDataItem(items);
  return baseDataItem
}

/** 获取所有的base信息通过KEY:VALUE形式 */
export const getAllBaseData = () => {
  const baseDataItem: any = {}
  let resData = {
    baseData: {
      items: [],
    },
    dictionaryCategory: {
      items: [],
    },
    dictionaryData: [],
    severityLevel: [],
    organizationUnit: [],
    organizationUsersTree: [],
    users: {
      items: [],
    },
    SessionState: "",
    userInOrganiza: [],
    organizationUsers: {},
    roleList: [],
  }
  if (!baseConfig.getLoginState()) {
    return;
  }
  let data = window.localStorage.getItem("commonDatas");
  if (data) {
    resData = Object.assign(resData, JSON.parse(data));
    data = null;
  }
  const { items = [] } = resData.baseData;
  /** 递归处理基础数据 */
  function getBaseDataItem(list: any[]) {
    list.forEach((item) => {
      baseDataItem[item.id || ""] = item;
      if (item.children) {
        getBaseDataItem(item.children);
      }
    });
  }
  getBaseDataItem(items);

  return baseDataItem
}
export function getCurrentTime() {
  const date = moment().format("YYYY-MM-DD HH:mm:ss")
  return date
  //
}
export function getCurrentDate() {
  const date = moment().format("YYYY-MM-DD")
  return date
  //
}
/** 获取当前用户
 * @isObj 是否返回整个对象
 */
export function getCurrentUser(isObj = false): { id?: string; signature?: string; name?: string } {
  let userInfo: any = window.localStorage.getItem("userInfo");
  if (userInfo) {
    userInfo = JSON.parse(userInfo);
    if (isObj) {
      return userInfo
    }
    const name = userInfo.surname + userInfo.name
    return {
      id: userInfo.id,
      signature: userInfo.signature || name,
      name: name
    }
  }

  return {
    id: undefined,
    signature: undefined,
    name: ''
  }
}
/** 获取当前部门 */
export function getCurrentOrg() {
  let userOrg: any = window.localStorage.getItem("userInOrganiza");
  if (userOrg) {
    userOrg = JSON.parse(userOrg);
    return userOrg.length ? userOrg[0] : { id: undefined }
  }
  return {
    id: undefined
  }
}

