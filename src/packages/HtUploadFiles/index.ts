/*
 * @Descripttion:
 * @version:
 * @Author: hutao
 * @Date: 2021-11-15 15:00:57
 * @LastEditors: hutao
 * @LastEditTime: 2022-02-11 14:26:45
 */
import HtUploadFiles from "./index.vue";
(HtUploadFiles as any).install = function (Vue: any) {

  Vue.component("HtUploadFiles", HtUploadFiles);
};
export default HtUploadFiles;