/*
 * @Descripttion: 基础数据展示组件
 * @version: 
 * @Author: hutao
 * @Date: 2022-04-12 17:34:51
 * @LastEditors: hutao
 * @LastEditTime: 2023-04-24 16:07:02
 */

import HtPopover from "./index.vue";
(HtPopover as any).install = function (Vue: any) {

  Vue.component("HtPopover", HtPopover);
};
export default HtPopover;