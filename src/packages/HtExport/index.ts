/*
 * @Descripttion:导出公共组件Excel
 * @version:
 * @Author: hutao
 * @Date: 2021-11-15 15:00:57
 * @LastEditors: hutao
 * @LastEditTime: 2021-12-19 13:38:48
 */
import HtExport from "./index.vue";

(HtExport as any).install = function (Vue: any) {

  Vue.component("HtExport", HtExport);
};

export default HtExport;