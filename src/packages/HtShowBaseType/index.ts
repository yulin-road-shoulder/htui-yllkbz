/*
 * @Descripttion: 基础数据展示组件
 * @version: 
 * @Author: hutao
 * @Date: 2022-04-12 17:34:51
 * @LastEditors: hutao
 * @LastEditTime: 2022-05-15 14:54:08
 */

import HtShowBaseType from "./index.vue";
(HtShowBaseType as any).install = function (Vue: any) {

  Vue.component("HtShowBaseType", HtShowBaseType);
};
export default HtShowBaseType;