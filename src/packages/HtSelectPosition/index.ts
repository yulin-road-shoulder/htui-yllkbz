/*
 * @Descripttion:选择资产单位
 * @version:
 * @Author: hutao
 * @Date: 2021-11-15 15:00:57
 * @LastEditors: hutao
 * @LastEditTime: 2023-01-15 14:45:42
 */
import HtSelectPosition from "./index.vue";
(HtSelectPosition as any).install = function (Vue: any) {

  Vue.component("HtSelectPosition", HtSelectPosition);
};
export default HtSelectPosition;