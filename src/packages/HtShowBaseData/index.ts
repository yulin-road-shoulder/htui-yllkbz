/*
 * @Descripttion: 基础数据展示组件
 * @version: 
 * @Author: hutao
 * @Date: 2022-04-12 17:34:51
 * @LastEditors: hutao
 * @LastEditTime: 2022-04-12 17:43:38
 */

import HtShowBaseData from "./index.vue";
(HtShowBaseData as any).install = function (Vue: any) {

  Vue.component("HtShowBaseData", HtShowBaseData);
};
export default HtShowBaseData;