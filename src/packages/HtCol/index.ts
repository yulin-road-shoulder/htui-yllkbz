/*
 * @Descripttion: 基础数据展示组件
 * @version: 
 * @Author: hutao
 * @Date: 2022-04-12 17:34:51
 * @LastEditors: hutao
 * @LastEditTime: 2023-04-21 16:35:06
 */

import HtCol from "./index.vue";
(HtCol as any).install = function (Vue: any) {

  Vue.component("HtCol", HtCol);
};
export default HtCol;