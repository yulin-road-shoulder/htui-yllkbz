/*
 * @Descripttion:
 * @version:
 * @Author: hutao
 * @Date: 2021-11-15 15:00:57
 * @LastEditors: hutao
 * @LastEditTime: 2021-12-21 14:15:10
 */
import HtUpload from "./index.vue";
(HtUpload as any).install = function (Vue: any) {

  Vue.component("HtUpload", HtUpload);
};
export default HtUpload;