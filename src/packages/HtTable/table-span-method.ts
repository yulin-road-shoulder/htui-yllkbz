/*
 * @Descripttion: 
 * @version: 
 * @Author: hutao
 * @Date: 2023-04-09 14:33:12
 * @LastEditors: hutao
 * @LastEditTime: 2023-04-10 18:22:09
 */
/**  处理table里面根据字段获取对应的值 */
export function getPropByPath(obj: any, path: string, strict = true) {
  let tempObj = obj;
  path = path.replace(/\[(\w+)\]/g, '.$1');
  path = path.replace(/^\./, '');

  const keyArr = path.split('.');
  let i = 0;
  for (let len = keyArr.length; i < len - 1; ++i) {
    if (!tempObj && !strict) break;
    const key = keyArr[i];
    if (key in tempObj) {
      tempObj = tempObj[key];
    } else {
      if (strict) {
        //throw new Error(`table中${path}字段发生错误,请检查`);
      }
      break;
    }
  }
  // return {
  //   o: tempObj,
  //   k: keyArr[i],
  //   v: tempObj ? tempObj[keyArr[i]] : null,
  // };
  return tempObj ? tempObj[keyArr[i]] : null;
}
/**
 * 合并相同数据，导出合并行所需的方法(只适合el-table)
 * @param {Array} dataArray el-table表数据源
 * @param {Array} mergeRowProp 合并行的列prop
 * @param {Array} sameRuleRowProp 相同合并规则行的列prop
 * @param {Object} connentProp 某个字段的合并基于其余字段的key是否相同
 */
export function getSpanMethod(dataArray: any[], mergeRowProp: string[], sameRuleRowProp: string[], connentProp: any) {

  /**
   * 要合并行的数据
   */
  const rowspanNumObject: any = {};

  //初始化 rowspanNumObject
  mergeRowProp.map(item => {
    rowspanNumObject[item] = new Array(dataArray.length).fill(1, 0, 1).fill(0, 1);
    rowspanNumObject[`${item}-index`] = 0;
  });
  function isConnent(strArr: string[], num: number) {
    let isConnn = true;
    for (let i = 0; i < strArr.length; i++) {
      if (getPropByPath(dataArray[num], strArr[i]) !== getPropByPath(dataArray[num - 1], strArr[i])) {
        isConnn = false
      }
    }
    return isConnn
  }
  //计算相关的合并信息
  for (let i = 1; i < dataArray.length; i++) {
    mergeRowProp.map(key => {
      const index = rowspanNumObject[`${key}-index`];

      // if (dataArray[i][key] === dataArray[i - 1][key]) {
      //   rowspanNumObject[key][index]++;
      // } else {
      //   rowspanNumObject[`${key}-index`] = i;
      //   rowspanNumObject[key][i] = 1;
      // }
      if (getPropByPath(dataArray[i], key) === getPropByPath(dataArray[i - 1], key)) {
        const connetKey = connentProp[key] as any;

        if (connetKey) {
          const connetKeyArr = connetKey.split(",");
          if (isConnent(connetKeyArr, i)) {
            rowspanNumObject[key][index]++;
          }
          // if (getPropByPath(dataArray[i], connetKey) === getPropByPath(dataArray[i - 1], connetKey)) {

          // }
          else {
            rowspanNumObject[`${key}-index`] = i;
            rowspanNumObject[key][i] = 1;
          }

        }
        else {
          rowspanNumObject[key][index]++;
        }

      } else {
        rowspanNumObject[`${key}-index`] = i;
        rowspanNumObject[key][i] = 1;
      }

    });
  }

  /**
   * 添加同规则合并行的数据
   */
  if (sameRuleRowProp !== undefined) {
    const k = Object.keys(rowspanNumObject).filter(key => {
      if (!key.includes('index')) {
        return key
      }
    })[0]
    for (const prop of sameRuleRowProp) {
      rowspanNumObject[prop] = rowspanNumObject[k]
      rowspanNumObject[`${prop}-index`] = rowspanNumObject[`${k}-index`]
      mergeRowProp.push(prop)
    }
  }

  /**
   * 导出合并方法
   */
  const spanMethod = function ({ row, column, rowIndex, columnIndex }: any) {
    // const colData = getPropByPath(row, column['property'])
    if (mergeRowProp.includes(column['property'])) {
      const rowspan = rowspanNumObject[column['property']][rowIndex];
      if (rowspan > 0) {
        return { rowspan: rowspan, colspan: 1 }
      }
      return { rowspan: 0, colspan: 0 }
    }
    return { rowspan: 1, colspan: 1 }
  };
  return spanMethod;
}
