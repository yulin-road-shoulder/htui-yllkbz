/*
 * @Descripttion:
 * @version:
 * @Author: hutao
 * @Date: 2021-11-15 15:00:57
 * @LastEditors: hutao
 * @LastEditTime: 2023-04-09 10:42:47
 */
import HtTable from "./index.vue";
//import HtTable from "./table.vue";
(HtTable as any).install = function (Vue: any) {

  Vue.component("HtTable", HtTable);
};
export default HtTable;