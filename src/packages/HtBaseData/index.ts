/*
 * @Descripttion: 基础数据展示组件
 * @version: 
 * @Author: hutao
 * @Date: 2022-04-12 17:34:51
 * @LastEditors: hutao
 * @LastEditTime: 2022-04-13 09:28:44
 */

import HtBaseData from "./index.vue";
(HtBaseData as any).install = function (Vue: any) {

  Vue.component("HtBaseData", HtBaseData);
};
export default HtBaseData;