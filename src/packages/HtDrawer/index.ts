/*
 * @Descripttion: 基础数据展示组件
 * @version: 
 * @Author: hutao
 * @Date: 2022-04-12 17:34:51
 * @LastEditors: hutao
 * @LastEditTime: 2022-09-28 11:18:26
 */

import HtDrawer from "./index.vue";
(HtDrawer as any).install = function (Vue: any) {

  Vue.component("HtDrawer", HtDrawer);
};
export default HtDrawer;