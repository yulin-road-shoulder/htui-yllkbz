/*
 * @Descripttion: 基础数据展示组件
 * @version: 
 * @Author: hutao
 * @Date: 2022-04-12 17:34:51
 * @LastEditors: hutao
 * @LastEditTime: 2023-04-23 13:44:24
 */

import HtBread from "./index.vue";
(HtBread as any).install = function (Vue: any) {

  Vue.component("HtBread", HtBread);
};
export default HtBread;