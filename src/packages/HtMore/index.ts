/*
 * @Descripttion: 更多三个点功能
 * @version: 
 * @Author: hutao
 * @Date: 2022-04-12 17:34:51
 * @LastEditors: hutao
 * @LastEditTime: 2023-01-08 16:16:28
 */

import HtMore from "./index.vue";
(HtMore as any).install = function (Vue: any) {

  Vue.component("HtMore", HtMore);
};
export default HtMore;