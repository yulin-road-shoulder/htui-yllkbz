/*
 * @Descripttion: 
 * @version: 
 * @Author: hutao
 * @Date: 2021-11-15 14:41:40
 * @LastEditors: hutao
 * @LastEditTime: 2023-03-07 17:47:41
 */
import Vue from "vue";
import App from "./App.vue";
import VueRouter from "vue-router";
import routes from "./router";
import store from "./store";
import ElementUI from "element-ui";
// import HtUi from "htui-yllkbz";
import "./styles.scss";
import { _axios, baseConfig } from "vue-kst-auth";
// import axios from "axios";
Vue.use(ElementUI);
Vue.use(VueRouter);
// Vue.use(HtUi);

/** 设置axios返回类型 */
Vue.config.productionTip = false;
/** 获取配置文件 */
_axios.get(`${process.env.BASE_URL}/baseConfig.json`).then((res: any) => {
  /** 将配置文件转换赋值 */
  store.commit("setBaseData", res.data);

  const router = new VueRouter({
    mode: "history",
    base: res.data.baseUrl,
    routes
  });
  router.beforeEach((to: any, from: any, next: any) => {
    /* 路由发生变化修改页面meta */
    if (to.meta.content) {
      const head = document.getElementsByTagName("head");
      const meta = document.createElement("meta");
      meta.content = to.meta.content;
      head[0].appendChild(meta);
    }
    /* 路由发生变化修改页面title */
    if (to.meta.title) {
      document.title = to.meta.title;
    }
    if (baseConfig.getLoginState() || /login/.test(to.path)) {
      // 授权配置
      baseConfig.setOAuthConfig(store.getters.getBaseData("oAuthConfig"));
      baseConfig.setLoginTitle(store.getters.getBaseData("enterpriseName")); // 企业名称
      // 设置验证租户有效性地址
      baseConfig.setIsTenantAvailable(
        store.getters.getBaseData("isTenantAvailable")
      );
      // 设置登录地址
      baseConfig.setLoginUrl(store.getters.getBaseData("loginUrl"));
      // 设置获取当前登录租户相关信息地址
      baseConfig.setcurrentLoginInfoUrl(
        store.getters.getBaseData("currentLoginInfoUrl")
      );
      next();
    } else {
      next(false);
      //next("/login");
    }
  });

  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount("#app");
});
