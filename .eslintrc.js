/*
 * @Descripttion: 
 * @version: 
 * @Author: hutao
 * @Date: 2021-11-15 14:41:40
 * @LastEditors: hutao
 * @LastEditTime: 2023-02-15 11:27:34
 */
module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    "plugin:vue/essential",
    // "eslint:recommended",
    "@vue/typescript/recommended"
  ],
  parserOptions: {
    ecmaVersion: 2020
  },
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
    "@typescript-eslint/no-explicit-any": ["off"]
  }
};
